import { Request, Response } from "express";
import { SummonerService } from '../services/SummonerService';
import { getSummonerIconUrl } from "../helpers/common";

const currentElo = async (req: Request, res: Response) => {
  const { summonerName } = req.params;
  const currentEloInfo = await SummonerService.getLeagueEntriesBySummonerName(summonerName);
  res.json(currentEloInfo);
};

const lastMatches = async (req: Request, res: Response) => {
  const { summonerName } = req.params;
  const summonerInfo = await SummonerService.getSummonerInfo(summonerName);
  const gamesInfo = await SummonerService.getLastGamesBySummonerName(summonerName);
  const gamesSummary = gamesInfo.map(gameInfo => {
    const participanItem = gameInfo.info.participants.find(participant => participant.puuid === summonerInfo.puuid);
    return {
      championPlayed: participanItem!.championName,
      win: participanItem!.win,
      kills: participanItem!.kills,
      deaths: participanItem!.deaths,
      assists: participanItem!.assists,
      individualPosition: participanItem!.individualPosition,
      lane: participanItem!.lane,
      largestKillingSpree: participanItem!.largestKillingSpree,
      role: participanItem!.role,
      kda: (participanItem?.kills! + participanItem?.assists!) / participanItem?.deaths!
    }
  })
  const championSummary = gamesSummary.reduce((acc, game) => {
    const { win, championPlayed } = game;
    if (!acc[championPlayed]) {
      acc[championPlayed] = 0;
    }
    if (win) {
      acc[championPlayed]++;
    }
    return acc;
  }, {} as Record<string, number>);
  const toReturn = {
    games: gamesSummary,
    wins: gamesSummary.reduce((acc, game) => {
      if (game.win) return acc + 1;
      return acc;
    }, 0),
    losses: gamesSummary.reduce((acc, game) => {
      if (!game.win) return acc + 1;
      return acc;
    }, 0),
    kda: gamesSummary.reduce((acc, game) => acc + game.kda, 0) / 20,
    championSummary
  }
  
  res.json(toReturn);
};

const getSummonerInfo = async (req: Request, res: Response) => {
  const { summonerName } = req.params;
  const summonerInfo = await SummonerService.getSummonerInfo(summonerName);
  res.json({
    ...summonerInfo,
    summonerIconUrl: getSummonerIconUrl(summonerInfo.profileIconId + ''),
  })
}

export {
  currentElo ,
  lastMatches,
  getSummonerInfo
}