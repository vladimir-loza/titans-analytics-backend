export const leagueEntriesUnused: string[] = [
  'leagueId', 'summonerId', 'veteran', 'inactive', 'freshBlood', 'hotStreak'
];