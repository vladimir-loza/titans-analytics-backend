import { lanApiInstance } from "../config/apiInstance";
import { ChampionMasteriesResponse } from "../interfaces/services/championInterfaces";

export class ChampionService {
  static async getChampionsMasterys(puuid: string): Promise<ChampionMasteriesResponse[]> {  // * Arreglo de todos los campeones con sus maestrias del summoner
    const { data } = await lanApiInstance
      .get(`/champion-mastery/v4/champion-masteries/by-puuid/${puuid}`);
    return data;
  }
  static async getChampionMastery(puuid: string, champId: string): Promise<ChampionMasteriesResponse> { // * Arreglo de todos los campeones con sus maestrias del summoner
    const { data } = await lanApiInstance
      .get(`/champion-mastery/v4/champion-masteries/by-puuid/${puuid}/by-champion/${champId}`);
    return data;
  }
}