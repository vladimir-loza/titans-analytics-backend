import axios from "axios";
import { lanApiInstance } from "../config/apiInstance";
import { API_KEY_PARAM } from "../constants/riotApiConstants";
import { 
  SummonerEntriesResponse, 
  SummonerInfoResponse, 
  SummonerMatchResponse, 
  SummonerMatchTimeLineResponse 
} from "../interfaces/services/summonerInterfaces";
import { delay } from "../helpers/time";

export class SummonerService {
  static async getSummonerInfo(summonerName: string): Promise<SummonerInfoResponse> { // * Info básica del summoner
    const { data } = await lanApiInstance
      .get(`summoner/v4/summoners/by-name/${summonerName}`);
    return data;
  }

  /**
   * 
   * @param summonerId summonerId to search
   * @returns 
   */
  static async getLeagueEntries(summonerId: string): Promise<SummonerEntriesResponse[]> { // * Info. importante del Elo actual del summoner
    const { data } = await lanApiInstance
      .get(`league/v4/entries/by-summoner/${summonerId}`);
    return data;
  }

  static async getLeagueEntriesBySummonerName(summonerName: string): Promise<SummonerEntriesResponse[]> { // * Info. importante del Elo actual del summoner
    const summonerInfo = await SummonerService.getSummonerInfo(summonerName);
    const { data } = await lanApiInstance
      .get(`league/v4/entries/by-summoner/${summonerInfo.id}`);
    return data;
  }
  static async getMatches(summonerPuuid:string, start:number = 0, count:number = 20): Promise<string[]> {
    const { data } = await axios
     .get(`https://americas.api.riotgames.com/lol/match/v5/matches/by-puuid/${summonerPuuid}/ids?start=${start}&count=${count}&${API_KEY_PARAM}`);
     return data;
  }

  static async getMatch(matchId: string): Promise<SummonerMatchResponse> {
    const { data } = await axios
      .get(`https://americas.api.riotgames.com/lol/match/v5/matches/${matchId}?${API_KEY_PARAM}`);
    return data;
  }

  static async getMatchTimeLine(matchId:string): Promise<SummonerMatchTimeLineResponse> {
    const { data } = await axios
      .get(`https://americas.api.riotgames.com/lol/match/v5/matches/${matchId}/timeline?${API_KEY_PARAM}`);
    return data;
  }

  static async getLastGamesBySummonerName(summonerName: string): Promise<SummonerMatchResponse[]> {
    const summonerInfo = await SummonerService.getSummonerInfo(summonerName);
    const matchIds = await SummonerService.getMatches(summonerInfo.puuid);
    const toReturn = [];
    for (const matchId of matchIds) {
      await delay(500);
      const matchInfo = await SummonerService.getMatch(matchId);
      toReturn.push(matchInfo);
    }
    return toReturn;
  };
}