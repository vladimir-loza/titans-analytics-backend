import express from 'express';
import summonerRouter from './routes/summoner-routes';

const app = express();
const PORT = 3000;
const API_PREFIX = '/api/v1';

app.use(express.json());
app.use(`${API_PREFIX}/summoner`, summonerRouter);

app.listen(PORT, () => console.log(`Server running on port: ${PORT}`))
