import axios from "axios";
import { API_KEY, LAN2_BASE_URL, LAN_BASE_URL } from "../constants/riotApiConstants";

export const lanApiInstance = axios.create({
  baseURL: LAN_BASE_URL,
  headers: {
    'X-Riot-Token': API_KEY
  }
});

export const lan2ApiInstance = axios.create({
  baseURL: LAN2_BASE_URL,
  headers: {
    'X-Riot-Token': API_KEY
  }
});