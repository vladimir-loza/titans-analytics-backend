import express from "express";
import { currentElo, lastMatches, getSummonerInfo } from "../controllers/summoner-controllers";

const summonerRouter = express();

summonerRouter.get('/info/:summonerName', getSummonerInfo)
summonerRouter.get('/currentElo/:summonerName', currentElo)
summonerRouter.get('/lastGames/:summonerName', lastMatches)

export default summonerRouter;