export const secondsToMinutes = (seconds: number) => {
  const minutes: number = Math.floor(seconds / 60);
  const secondsLeft: number = seconds % 60;

  return `${minutes || '00'}:${secondsLeft || '00'}`
};

export const delay = (ms:number) => new Promise(resolve => setTimeout(resolve, ms));
