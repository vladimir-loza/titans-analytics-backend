import { SUMMONER_ICON_URL } from "../constants/riotApiConstants"

export const getSummonerIconUrl = (iconId: string) => `${SUMMONER_ICON_URL}${iconId}.png`;