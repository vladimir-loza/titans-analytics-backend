"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.lanApiInstance = void 0;
const axios_1 = __importDefault(require("axios"));
const riotApiConstants_1 = require("../constants/riotApiConstants");
exports.lanApiInstance = axios_1.default.create({
    baseURL: riotApiConstants_1.LAN_BASE_URL,
    headers: {
        'X-Riot-Token': riotApiConstants_1.API_KEY
    }
});
