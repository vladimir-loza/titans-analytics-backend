"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChampionService = void 0;
const apiInstance_1 = require("../config/apiInstance");
class ChampionService {
    static getChampionsMasterys(puuid = "") {
        return __awaiter(this, void 0, void 0, function* () {
            if (!puuid)
                return;
            const { data } = yield apiInstance_1.lanApiInstance
                .get(`/champion-mastery/v4/champion-masteries/by-puuid/${puuid}`);
            return data;
        });
    }
    static getChampionMastery(puuid = "", champId = "") {
        return __awaiter(this, void 0, void 0, function* () {
            if (!puuid || !champId)
                return;
            const { data } = yield apiInstance_1.lanApiInstance
                .get(`/champion-mastery/v4/champion-masteries/by-puuid/${puuid}/by-champion/${champId}`);
            return data;
        });
    }
}
exports.ChampionService = ChampionService;
